import csv
import pymongo

filename = "../data/task1.csv"
client = pymongo.MongoClient("mongodb://localhost:27017/")
my_db = client["assignment3"]
cursor = my_db["task1"]


def get_dict_from_csv(filename_):
    json_array = []
    with open(filename_, encoding='utf-8') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        for row in csv_reader:
            json_array.append(row)

    return json_array


def save_line_to_mongo(line):
    response = cursor.insert_one(line)
    print(f'Inserted line id - {response.inserted_id}')


def save_dump_to_mongo(lines):
    response = cursor.insert_many(lines)
    for inserted_id in response.inserted_ids:
        print(f'Inserted line id - {inserted_id}')


if __name__ == '__main__':
    tmp = get_dict_from_csv(filename)
    for line_ in tmp:
        save_line_to_mongo(line_)

    save_dump_to_mongo(tmp)
