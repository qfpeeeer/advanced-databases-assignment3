import pymongo
from pymongo.cursor import Cursor

client = pymongo.MongoClient("mongodb://localhost:27017/")
my_db = client["assignment3"]
cursor = my_db["task1"]


def print_iterable(iterable: Cursor):
    if iterable is None:
        print('Nothing to print')
        return

    while True:
        try:
            print(iterable.next())
        except StopIteration:
            return


def task1():
    # Выдать упорядоченный список URL ресурсов
    sorted_urls = cursor.find({}, {'url': 1, '_id': 0}).sort({'url': pymongo.ASCENDING})
    print_iterable(sorted_urls)

    # Выдать упорядоченный список IP-адресов пользователей, посетивших ресурс с заданным URL.
    random_url = sorted_urls[0]
    ips = cursor.find({'$eq': random_url}, {'ip': 1}).sort({'ip': pymongo.ASCENDING})
    print_iterable(ips)

    # Выдать упорядоченный список URL ресурсов, посещенных в заданный временной период.
    start_time = "2016-11-12T05:19:01.394Z"
    end_time = "2017-08-09T08:03:08.769Z"

    opened_urls = cursor.find({'timeStamp': {'$gt': start_time, '$lt': end_time}}, {'url': 1, '_id': 0})
    print_iterable(opened_urls)

    # Выдать упорядоченный список URL ресурсов, посещенных пользователем с заданным IPадресом.
    random_ip = ips[0]
    user_urls = cursor.find({'ip': {'$eq': random_ip}}, {'url': 1, '_id': 0})
    print_iterable(user_urls)


def task2():
    #  Выдать список URL ресурсов с указанием суммарной длительности посещения каждого ресурса, упорядоченный по убыванию.
    cursor.command('mapreduce', 'totalVisitTimeOfUrls', map='function () { emit(%s, %s); }',
                   reduce='function(key, values) { return Array.sum(values) / 1000; }', out='totalVisitTimeOfUrls')
    answer: Cursor = my_db['totalVisitTimeOfUrls'].find().sort({'value': pymongo.DESCENDING})
    print_iterable(answer)

    #  Выдать список URL ресурсов с указанием суммарного количества посещений каждого ресурса, упорядоченный по убыванию
    cursor.command('mapreduce', 'totalVisitCountOfUrls', map='function () { emit(%s, 1); }',
                   reduce='function(key, values) { return Array.sum(values); }', out='totalVisitCountOfUrls')
    answer: Cursor = my_db['totalVisitCountOfUrls'].find().sort({'value': pymongo.DESCENDING})
    print_iterable(answer)

    # Выдать список URL ресурсов с указанием количества посещений каждого ресурса в день за заданный период, упорядоченный URL ресурса и убыванию количества посещений.
    cursor.command('mapreduce', 'visitsCountOfUrlsInPeriod',
                   map="function () { if (%s >= %s && %s <= %s) emit(%s, 1); }",
                   reduce='function(key, values) { return Array.sum(values); }', out='visitsCountOfUrlsInPeriod')
    answer: Cursor = my_db['visitsCountOfUrlsInPeriod'].find().sort({'value': pymongo.DESCENDING})
    print_iterable(answer)

    #  Выдать список IP-адресов c указанием суммарного количества и суммарной длительности посещений ресурсов, упорядоченный по адресу, убыванию количества и убыванию длительности.
    map_function = '''
    function (){ 
    emit({ip: %s, url: %s}, {totalCount: 1, totalDuration: %s}); 
    }'''

    reduce_function = '''
    function(key, values) {
    var totalCount = 0;
    var totalDuration = 0; 
    for (var i in values) {
    totalCount += values[i].totalCount;
    totalDuration += values[i].totalDuration;}
    return {totalCount: totalCount, totalDuration: totalDuration}; }
    '''
    cursor.command('mapreduce', 'totalVisitsCountAndTimeOfIps',
                   map=map_function,
                   reduce=reduce_function, out='totalVisitsCountAndTimeOfIps')
    answer: Cursor = my_db['totalVisitsCountAndTimeOfIps'].find().sort(
        {'url': pymongo.DESCENDING, 'totalCount': pymongo.DESCENDING, 'totalDuration': pymongo.DESCENDING})
    print_iterable(answer)


if __name__ == '__main__':
    task1()
    task2()
